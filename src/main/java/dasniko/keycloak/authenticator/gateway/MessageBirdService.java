package dasniko.keycloak.authenticator.gateway;

import com.messagebird.MessageBirdClient;
import com.messagebird.MessageBirdServiceImpl;
import com.messagebird.exceptions.GeneralException;
import com.messagebird.exceptions.UnauthorizedException;
import com.messagebird.objects.MessageResponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageBirdService implements SmsService {

	private final String apiKey;
	private final MessageBirdServiceImpl messageBirdService;
	private final MessageBirdClient messageBirdClient;
	private final String senderId;

	MessageBirdService(Map<String, String> config) {
		this.apiKey = config.get("apikeyMessageBird");
		// Create a MessageBirdService
		this.messageBirdService = new MessageBirdServiceImpl(this.apiKey);
		// Add the service to the client
		this.messageBirdClient = new MessageBirdClient(this.messageBirdService);
		this.senderId = config.get("senderId");
	}

	@Override
	public void send(String phoneNumber, String message) throws GeneralException, UnauthorizedException {
		// convert String number into acceptable format
		BigInteger phoneNumberInt = new BigInteger(phoneNumber);
		final List<BigInteger> phones = new ArrayList<BigInteger>();
		phones.add(phoneNumberInt);

		final MessageResponse response = this.messageBirdClient.sendMessage(this.senderId, message, phones); }
}
